data "aws_iam_account_alias" "current" {}

data "aws_caller_identity" "current" {}

data "aws_vpc" "netbox" {
  filter {
    name = "vpc-id"
    values = [var.vpc_id]
  }
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.netbox.id
}

data "aws_route53_zone" "zone" {
  count = var.domain_name == "example.com" ? 0 : 1

  name         = "${var.domain_name}."
  private_zone = false
}