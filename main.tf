terraform {
  required_version = "= 0.12.29"
}

provider "aws" {
  version = "~> 2.8"
  region  = var.aws_region
}

locals {
  caller = regex("^.*/", data.aws_caller_identity.current.arn)
}

module "ec2-instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.15.0"

  instance_count = var.instance_count

  name          = var.host_name
  ami           = var.AMI
  instance_type = var.InstanceType
  key_name      = var.KeyName
  subnet_id     = tolist(data.aws_subnet_ids.all.ids)[0]
  vpc_security_group_ids      = [module.security-group.this_security_group_id]
  associate_public_ip_address = true
  iam_instance_profile = null

  user_data = templatefile("${path.module}/files/user_data.tpl", {
    Environment = var.Environment
    Hostname    = var.host_name
  })

  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 10
    },
  ]

//  ebs_block_device = [
//    {
//      device_name = "/dev/sdf"
//      volume_type = "gp2"
//      volume_size = 5
//      encrypted   = true
//    }
//  ]

  tags = merge(var.default_tags, {
    acct      = data.aws_iam_account_alias.current.account_alias,
    owner     = local.caller
    autostart = true
  })
}

resource "aws_route53_record" "netbox" {
  count   = var.domain_name == "example.com" ? 0 : var.instance_count

  zone_id = data.aws_route53_zone.zone[0].zone_id
  name    = "${var.host_name}-${count.index+1}.${var.domain_name}"
  type    = "A"
  ttl     = 300
  records = [module.ec2-instance.public_ip[count.index]]
}
