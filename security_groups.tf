module "security-group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "3.13.0"

  name        = "netbox-simple-sg"
  description = "Security group for Netbox"
  vpc_id      = data.aws_vpc.netbox.id

  ingress_cidr_blocks = var.AllowedIngress
  ingress_rules       = ["http-80-tcp", "https-443-tcp", "ssh-tcp", "all-icmp"]
  egress_rules        = ["all-all"]
}