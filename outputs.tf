#Account
output "account_id" {
  value = data.aws_caller_identity.current.account_id
}

output "private_ip" {
  value = module.ec2-instance.private_ip
}

output "private_dns" {
  value = module.ec2-instance.private_dns
}

output "public_ip" {
  value = module.ec2-instance.public_ip
}

output "public_dns" {
  value = module.ec2-instance.public_dns
}