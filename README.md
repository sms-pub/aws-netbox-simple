# aws-netbox

NetBox is an open source web application designed to help manage and document computer networks.

## Getting Started
Rename or copy the auto.tfvars.sample to {environment}.auto.tfvars inside the root directory and set values for varibles
as needed.
* See variables.tf file for a list of variables that can be set.

You should update the following at minimum;
- KeyName - SSH Key to use with EC2 instances
- domain_name - Route53 hosted zone

## Using Terraform, deploy into AWS.
Use environment to set AWS credentials.
```bash
export AWS_ACCESS_KEY_ID="SAMPLEACCESSKEY"
export AWS_SECRET_ACCESS_KEY="SAMPLESECRETACCESSKEY"
export AWS_REGION="us-east-1"
```

Initialize local Terraform workspace and pull external modules.
```bash
terraform init
terraoform get
```

Make sure you're in the root of the repository and run;
```bash
terraform plan
```

Apply plan to AWS.
```bash
terraform apply
```