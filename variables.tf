variable "aws_region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}

variable "vpc_id" {
  type    = string
  description = "ID of the VPC to use"
  default = "default"
}

variable "host_name" {
  type = string
  description = "Instance hostname"
  default     = "netbox-simple"
}

variable "domain_name" {
  type = string
  description = "DNS Domain"
  default     = "example.com"
}

variable "AMI" {
  type        = string
  description = "AMI for the Netbox EC2 instances"
  default    = "ami-0f1812e83bc7f1f37" # AWS Marketplace Netbox v2.8.8
}

variable "KeyName" {
  type        = string
  description = "SSH key name for the Netbox EC2 instances"
}

variable "Environment" {
  type        = string
  description = "Environment this application is in"
  default     = "dev"
}

variable "instance_count" {
  type        = number
  description = "Number of EC2 instances to create"
  default     = 1
}

variable "InstanceType" {
  type        = string
  description = "EC2 instance type used for the Netbox application"
  default     = "t2.micro"
}

variable "AllowedIngress" {
  type        = list
  description = "List of hosts allowed to communicate with the Netbox ELB"
  default     = ["0.0.0.0/0"]
}

variable "default_tags" {
  type        = map(string)
  description = "Map of default tags"
  default = {
    env      = "dev"
    org      = "sms"
    tf-owned = "true"
    version  = "0.1"
  }
}
