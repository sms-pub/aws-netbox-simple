#!/usr/bin/env bash
echo "127.0.0.1 ${Hostname}" >> /etc/hosts
echo "${Hostname}" > /etc/hostname
timedatectl set-timezone UTC
apt-get update -y
apt-get upgrade -y